# Virtualbox Ubuntu 22.04 TPM2 simulator 

Script for setting up a TPM2 simulator on virtualbox with ubuntu 22.04 Server or Desktop

## Installation

This script was tested on a fresh install of ubuntu 22.04 Server on Virtualbox 6.1. It was also tested with ubuntu 22.04 Desktop.
Run setup_tpm2.sh to install everything needed to run a TPM2 simulator using IBM's TPM2 simulator
(https://sourceforge.net/projects/ibmswtpm2/) and Linux TPM2 & TSS2 Software (https://github.com/tpm2-software)

```bash
./setup_tpm2.sh
```

## Testing the installation
The following command can be run in a single bash console. Alternatively, tpm_server and tpm2-abrmd can be run in separate windows to monitor their status.

```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib64
# Start the TPM2 simulator
~/ibmswtpm2-tpm2/src/tpm_server &
TPMSERVER_PID=$!

# Start the Access Broker and Resource Management (ABRM) in the background
sudo -b -u tss tpm2-abrmd --tcti=mssim

# Test with pcrread... (uncomment the following line)
# tpm2_pcrread

# Test by generating a random number
./test_tpm2.sh

kill $TPMSERVER_PID
# kill the ABRM (started with user tss)
sudo killall -u tss

# Note: If you see the following error, it is due to a missing LD_LIBRARY_PATH.
# Be sure to add this to your .bashrc
# ERROR:tcti:src/tss2-tcti/tctildr.c:428:Tss2_TctiLdr_Initialize_Ex() Failed to instantiate TCTI
# ERROR: Could not load tcti, got: "tabrmd:bus_name=com.intel.tss2.Tabrmd"
# $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib64
```

## Simulator as a service
Once the installation can be tested on the command line, it can be installed and run as a service. Service definitions
are found in /lib/systemd/system.  

```bash
sudo cp tpm2-abrmd.service /lib/systemd/system/.
sudo cp tpm-server.service /lib/systemd/system/.

# reload daemons
systemctl daemon-reload

# start the services
systemctl start tpm-server.service
systemctl start tpm2-abrmd.service

# check its status, if all is fine should be in Active state
service tpm-server status
service tpm2-abrmd status

# Test by generating a random number
./test_tpm2.sh

```

## References
References

https://sourceforge.net/projects/ibmswtpm2/

https://github.com/tpm2-software/tpm2-tss

https://github.com/tpm2-software/tpm2-abrmd/blob/master/INSTALL.md

https://github.com/tpm2-software/tpm2-tss-engine

https://github.com/tpm2-software/tpm2-tools

https://francislampayan.medium.com/how-to-setup-tpm-simulator-in-ubuntu-20-04-25ec673b88dc

