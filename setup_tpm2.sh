sudo apt -y update
sudo apt -y install \
  autoconf-archive \
  libcmocka0 \
  libcmocka-dev \
  procps \
  iproute2 \
  build-essential \
  git \
  pkg-config \
  gcc \
  libtool \
  automake \
  libssl-dev \
  uthash-dev \
  autoconf \
  doxygen \
  libjson-c-dev \
  libini-config-dev \
  libcurl4-openssl-dev \
  libltdl-dev \
  libglib2.0-dev \
  expect

# liburiparser-dev \
# libdbus-1-dev  dbus-x11 \
# libcurl4-gnutls-dev libgcrypt20-dev  \
function check_continue() {
  while true; do
    read -p "Do you want to continue? (y/n) " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
  done
}

# TPM simulator
# The following does not work because it is version 1661, which does not support openssl 3.0, which is what ubuntu 22.04 comes with
# mkdir ibmtpm && cd ibmtpm
# wget https://sourceforge.net/projects/ibmswtpm2/files/latest/download -O ibmtpm.tar.gz
# tar -zxvf ibmtpm.tar.gz
# Instead, get the latest commit from the repo (which is in the "next" branch) (status after teh git:)
# git status
# On branch next
# Your branch is up to date with 'origin/next'.
cd
git clone https://git.code.sf.net/p/ibmswtpm2/tpm2 ibmswtpm2-tpm2
cd ibmswtpm2-tpm2
cd src
make -j5
export PATH=$PATH:$PWD
# This creates an executable "tpm_server" in this directory
echo "tpm_server is installed. Continue if the output looks good..."
check_continue
sudo cp tpm_server /usr/local/bin/.

# https://tpm2-tools.readthedocs.io/en/latest/INSTALL/
# Build and install the TSS libraries on ubuntu 22.04 (server)
# https://github.com/tpm2-software/tpm2-tss/blob/master/INSTALL.md

# add the following to .bashrc
export PKG_CONFIG_PATH=~/tpm2-abrmd/dist
export TPM2TOOLS_TCTI="tabrmd:bus_name=com.intel.tss2.Tabrmd"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib64:$LD_LIBRARY_PATH

# Tpm2-tss
cd
git clone https://github.com/tpm2-software/tpm2-tss.git
cd tpm2-tss
./bootstrap
./configure --enable-unit --enable-integration 
make check
echo "TPM2 software stack (TSS) is installed. Continue if all the tests pass...."
check_continue
sudo make install

# Tpm2-abrmd
# TPM2 Access Broker & Resource Management Daemon implementing the TCG spec.
# sudo apt install tpm2-abrmd
# --with-dbuspolicydir this is the directory where a policy that will allow tss user account to claim a name on the D-Bus system bus.
# --with-systemdsystemunitdir: systemd unit directory that is needed for tpm2-abrmd daemon to be started as part of the boot process of your unit.
cd
git clone https://github.com/tpm2-software/tpm2-abrmd.git
cd tpm2-abrmd
./bootstrap
./configure --enable-unit --enable-integration --with-dbuspolicydir=/etc/dbus-1/system.d --with-systemdsystemunitdir=/usr/lib/systemd/system --libdir=/usr/lib64 --prefix=/usr 
make -j5
#run unit and integration tests
make check
echo "TPM2 Access Broker & Resource Management (tpm2-abrmd) is installed. Continue if the tests pass..."
check_continue
sudo make install
sudo ldconfig

# Tpm2-tools
# sudo apt install tpm2-tools
cd
git clone https://github.com/tpm2-software/tpm2-tools.git
cd tpm2-tools
./bootstrap
./configure --enable-unit --enable-integration  --prefix=/usr
make -j5
make check
echo "TPM2 tools is installed. Continue if all the tests pass."
check_continue
sudo make install

# Note that you need to reboot after adding the user to a group!!

echo "Add the following variables assignments to your .bashrc or you'll be sorry."
echo export PKG_CONFIG_PATH=~/tpm2-abrmd/dist
echo export TPM2TOOLS_TCTI="tabrmd:bus_name=com.intel.tss2.Tabrmd"
echo export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib64:$LD_LIBRARY_PATH

sudo usermod -a -G tss $USER
echo "User is added to the tss group. You need to reboot for it to take effect."
echo "If you agree to continue, the VM will reboot. If you do not continue, the script will exit."
check_continue
sudo reboot
